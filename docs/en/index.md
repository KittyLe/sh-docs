# Welcome!

Welcome to my version of a guide to SHFW/SHU. This guide assumes you are new to hacking, or have used zips in the past.

Most of the info you will read here is from [RollerPlausch](https://rollerplausch.com), Telegram, or my knowledge.

## Which scooters are supported?
The scooters supported are:

- Ninebot MAX G30, G30D II, G30Lx
- Ninebot D series
- Ninebot E series
- Ninebot ESx series
- Ninebot F series
- Xiaomi Mi 1s
- Xiaomi Mi Pro 2
- Xiaomi Mi 3
- Xiaomi Pro

## Before you start...

There are some terms you need to know about.

- **FW**: short for **F**irm**w**are
- **SH**: short for **S**cooter**H**acking
- **Firmware**: software, which is being used on the scooter
- **CFW**: Custom firmware, which means that it's been modified
- **SHFW**: ScooterHacking firmware, allows you to change settings on-the-fly (instantly) via the app
- **SHU**: ScooterHacking Utility, allows you to flash SHFW on your scooter, as well as allow you to change settings in SHFW
- **BLE**: short for **B**luetooth **L**ow **E**nergy, in actuality it means the dashboard of your scooter, or its firmware
- **BMS**: short for **B**attery **M**anagement **S**ystem, means your scooter battery's protection system, or its firwmare
- **ESC** or **DRV**: ESC stands for **E**lectronic **S**peed **C**ontrol, means your scooter's main controller, which drives the motor, or its firmware

BLE and DRV must function in order to proceed.

Xiaomi Mi 3 Lite, 4, 4 Ultra and 4 Lite are **not** supported, as they are made by Brightway/Navee, which use a completely different layout and protocol. Mi 4 Pro is **not** supported as well.

## What differences are there from ZIP files?

SHFW doesn't require you to reflash every time you change your settings. Instead, you are able to instantly apply your settings by changing them in the app. No reboots, waiting for the scooter to flash, realizing that it is not what you wanted, reconfiguring, reflashing...

## I'm ready!
Go to [this page](starting/index.md) to start.