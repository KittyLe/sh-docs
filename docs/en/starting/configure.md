# Configuring your scooter

Head over to the Config tab. You might be startled by the amount of things to set up, but do not worry: it is not as hard as it seems.

## Profiles

The first thing you will see is the Profile Editor. It allows you to set up multiple profiles for different situations. For example, you can set up a profile with stock values, so in case something happens and you need the scooter to act as if it is stock, you will be able to switch to that profile. I will explain how to switch between profile later in the guide, so keep reading. :D

The Manage Profile button allows you to rename the profile you have selected right now, copy it into another profile, or reset the profile.

## Throttle and brake

This is the fun part: you get to set up how your scooter will act in different modes. Specifically, the current that will be given to the motor based on your throttle/brake position.

First, I will go over all the settings, then we'll set it up.

### Selected mode

Should be pretty obvious, there are 4 modes: Sports, Drive, Eco, and Brake. S/D/Eco are pretty much the same, Brake defines how strong the motor will brake when you do.

### Speed limit

Also pretty obvious, limits the speed. Off means no limit.

### Throttle mode

This is where it gets kind of interesting, your first big choice. 

#### Speed-based
This mode is more like the original firmware: the scooter will try to stay at a speed, set by your throttle position.

The settings for speed-based mode are:

- Power limit: defines the absolute maximum of current that the DRV will give the motor.
- Current smoothness: defines how smooth the DRV will try to reach the speed. The lower, the smoother.

But that's not what we're here for, right?

#### Power-based (DPC)

This is the big reason of why you install CFW as a whole: it's the real deal. Instead of your throttle defining the max speed, it now defines the amount of current flowing into the motor.

This mode has a current-to-throttle curve, which you need to set up yourself. Tap on that `Build curve` button, and put in some current value. The general safe value for every scooter is around 30A. You really shouldn't be putting in more, because the phase connectors (they connect the motor to the DRV) are only rated for 30A, and [might melt](../../photo_2023-11-06_04-13-53.jpg). The safe values are available [here](https://github.com/lekrsu/shfw-walkthrough/blob/main/README.md#usage).

### Motor start speed

Pretty self-explanatory as well, it is the speed, at which the motor will start spinning. Put that to whatever you like.

### Field Weakening

**WARNING**: Field weakening can damage the actual hardware of your DRV, not software. If you are not being careful, you might burn something.

Field weakening allows you to reach higher speeds, in expense of torque and efficiency/range. It takes significantly more current, but instead gives you a bit more speed. The field weakening values are available on the safe values page I linked before.

#### Enable by mode

Gives you three switches, allows you to decide in which modes to enable field weakening: Eco, Drive, or Sport. Personally, field weakening is not needed anywhere, other than Sport.

#### Start speed

The speed at which field weakening starts working. Set it to somewhere around 20 km/h.

#### Initial current

The current that is supplied instantly when field weakening is turned on. I like to set it to 3 A, because it gives a little boost, kinda like N2O in cars.

#### Variable current

This is, probably, the most important setting. It decides how much mA to add per each km/h. More info on the safe values page.

### Cruise control

TODO

### Modes

Allows you to set which mode to use when you power on the scooter. `Last` means the mode the scooter was in before being turned **off**.

### Lights

Allows you to customize how the lights act.

#### Brake light mode

Gives you multiple choices:

- Default: the rear light flashes when the brake is pressed. The frequency is defined by `Brake light flash speed`
- Static: the rear light is working the same as the headlight. If the headlight is on, then the rear light is on as well
- Reversed: the rear light flashes when the brake is **not** pressed. The frequency is defined by `Brake light flash speed`
- Strobe: stroboscope-like effect. The frequency is defined by `Brake light flash speed`
- German: the rear light is always on, no matter if you are braking, or if the headlight is on/off.
- Car: the rear light behaves as on cars, when you press the brake, it comes on.

#### Brake light flash speed

A slider, which defines the speed with which the brake light flashes.

#### Always active brake light

Makes the rear light shine always, no matter anything.

#### Always on headlight

Makes the headlight shine always, no matter anything.

### User interface

This is where you get to change what is shown on your scooter's dashboard.

#### Main dash data

Here you can set what is shown on your dashboard, when your scooter is moving. Usually, you would choose the speed.

#### Idle dash data

Here is where you can set what is shown while the scooter is **idling** (not moving). By default, it is set to `Battery level`, but you can change it to whatever you want, or choose `Off` to simulate stock firmware.

#### Alternating dash data

This option sets additional data, that will be alternating with `Main dash data` while you are moving. This will also use red numbers, so don't think that your scooter is showing an error code.

#### Battery Bar data

This option allows you to change what exactly is shown on the battery bar (the bottom 5 segments) of your display. 

#### Beep type when entering profile

Makes your scooter beep, when changing between profiles. I like to set this to `None`, because my scooter won't bring up any suspicion when I change into the `Stock` profile.

#### No reboot/shutdown beep

This switch allows you to disable the reboot/shutdown beeps, which can sometimes be annoying.

### Profile triggers

TODO

### System settings

Allows you to change miscellaneous scooter settings.

#### Auto-shutdown delay

That delay is basically how long the scooter will stay on while idling.

#### Auto-shutdown delay when locked

Same as the previous one, except now when the scooter is locked.

#### Error suppression

Suppresses some errors. That's it.

#### Max ADC resistor divider voltage

Needed, if you swapped your ADC resistor. For the purposes of this guide, don't change that.

### Wheel size

If you ever change the tire to a bigger size one, your speed reading might be incorrect. This solves that issue.

### Speed Control Settings

For the purposes of this guide, leave this as it is. It is set up as it needs to be.

### BMS emulation

Useful, if you don't have BMS communication. Stops error 21 from coming on. 