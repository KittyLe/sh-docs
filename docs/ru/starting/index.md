# So, you are new to this whole thing.

First of all, you need to have an Android device on you. Period. iOS is not supported, and it is unlikely that it will be supported, because Apple locks down a lot of things.

If you do have an Android device, go to [this page](https://utility.cfw.sh/) to download the app. 

## I installed the app, what next?

After connecting to the scooter, you will need to check a few things.

1. Check the controller revision.

    There is a three-dots-menu at the top of the Info page. Tap on it, then tap on `Hardware details`.

    For Xiaomi only: 

    - If you see `mi_DRV_STM32`, that means you have an old v3.0 controller. This is a good thing, since you can proceed with flashing any firmware, including SHFW.
    - If you see `mi_DRV_GD32`, that means you have a **new** v3.1 controller. This is not entirely bad, but you need to be careful. Flashing anything older than `DRV319` (1s), `DRV247` (Pro 2), `DRV242` (Lite or Essential) or `DRV015` (Mi 3) **will** brick your scooter. If you do flash those versions, you will need to [ST-Link](../stlink/index.md) it.


2. The BLE version.

    If it's above 1.5.5, you will need to ST-Link your dashboard. Go to [this page](../stlink/index.md) for more info.

    Another solution would be to temporarily swap the dashboard module, if you have one laying around. Configuring SHFW later **will** work even with your old one.

3. This step is only for Mi 3 users.

    Go to the Flash tab, and tap on `Attempt downgrade exploit` to install a patched version of `DRV016`. This will remove the model lock, and allow you to flash SHFW.

After all this, you are ready to flash your scooter!

## Flashing

To flash your scooter to SHFW, you will need to go to the Flash tab, tap on `Load custom`, then choose `Install/update SHFW`. Depending on your scooter, you might get multiple choices. For now, pick the first one.

Keep your phone close to the scooter's dashboard while it's flashing! You could end up with a bricked scooter if the process gets interrupted, and you **will** need to [ST-Link](../stlink/index.md) your controller.

After it's done, congratulations! You have just installed SHFW on your scooter. 

### What happened? 

You might notice your scooter's dashboard is showing A0, or any other random number. This is **normal**, by default while idling, SHFW shows your battery percentage on the screen. A0 means 100%, and is **not** an error. Also, your scooter will not spin the wheel at all, this is because the throttle curves are set to 0 by default, so the scooter will not react to any inputs. I explain how to set this up in the next part of the guide.

With that being said, head over to [Configuring](configure.md).